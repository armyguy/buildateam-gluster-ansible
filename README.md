##To install glusterfs into your CentOS server you can use this role

The **gluster-role** is located in the folder **roles/gluster-server**

What this role is doing:

1. Installing **epel-release**
1. Downloading the official repo file and put it into the yum.repos.d directory
1. Installing **glusterfs-server**
1. Enabling and starting the service
1. Connecting the peers to the current server

To set the list of peers please redefine the variable **{{ gluster_peers }}**